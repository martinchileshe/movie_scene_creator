/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics.pkgpackage;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Martin
 */
public class DrawJPanel extends JPanel{
    // display image
    private double scaleValue = 1.0;
    public static ImageIcon image;
    public static BufferedImage bImage;
    public static Graphics g;
    
public void paintComponent( Graphics g )
        
{
    super.paintComponent( g );
    try{
    BufferedImage bImage = ImageIO.read(new File("C:\\Users\\Martin\\Documents\\NetBeansProjects\\Graphics Package\\src\\graphics\\pkgpackage\\img.png"));
    image = new ImageIcon(bImage);
    }catch(IOException e){
    }

    // the following values are used to center the image
    double spareWidth =
    getWidth() - scaleValue * image.getIconWidth();
    double spareHeight =
    getHeight() - scaleValue * image.getIconHeight();

    // draw image with scaled width and height
    g.drawImage( image.getImage(),
    (int) ( spareWidth ) / 2, (int) ( spareHeight ) / 2,
    (int) ( image.getIconWidth() * scaleValue ),
    (int) ( image.getIconHeight() * scaleValue ), this );




 } // end method paint
public static void update(ImageIcon image, int x, int y, int width, int height, DrawJPanel panel){
    // draw image with scaled width and height
    panel.getGraphics().drawImage(image.getImage(), x, y, width, height, panel);
}
 } // end class DrawJPanel
