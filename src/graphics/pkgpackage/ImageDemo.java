///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package graphics.pkgpackage;
//
///**
// *
// * @author Martin
// */
//import java.awt.*;
//import java.awt.image.*;
//import java.io.*;
//import javax.imageio.*;
//import javax.swing.*;
//
///**
// * A Java class to demonstrate how to load an image from disk with the
// * ImageIO class. Also shows how to display the image by creating an
// * ImageIcon, placing that icon an a JLabel, and placing that label on
// * a JFrame.
// * 
// * @author alvin alexander, alvinalexander.com
// */
//public class ImageDemo
//{
//  public static void main(String[] args) throws Exception
//  {
//    new ImageDemo("C:\\Users\\Martin\\Documents\\NetBeansProjects\\Graphics Package\\src\\graphics\\pkgpackage\\house.jpg");
//  }
//
//  public ImageDemo(final String filename) throws Exception
//  {
//    SwingUtilities.invokeLater(new Runnable()
//    {
//      public void run()
//      {
//          final JFileChooser fc = new JFileChooser();
////        fc.setCurrentDirectory();
//        //In response to a button click:
//        JFrame editorFrame = new JFrame("Image Demo");
//        JButton button = new JButton();
//        button.setText("Tyr me");
//        JPanel panel = new JPanel();
//        editorFrame.add(panel, BorderLayout.EAST);
//        editorFrame.add(button, BorderLayout.WEST);
//        editorFrame.pack();
//        editorFrame.setLocationRelativeTo(null);
//        editorFrame.setVisible(true);
//        int returnVal = fc.showOpenDialog(editorFrame);
//        File file = fc.getSelectedFile();
//        editorFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        
//        BufferedImage image = null;
//        try
//        {
//          image = ImageIO.read(file.getAbsoluteFile());
//        }
//        catch (Exception e)
//        {
//          e.printStackTrace();
//          System.exit(1);
//        }
//        ImageIcon imageIcon = new ImageIcon(image);
//        JLabel jLabel = new JLabel();
//        jLabel.setIcon(imageIcon);
//        panel.add(jLabel, BorderLayout.CENTER);
////        editorFrame.getContentPane().add(jLabel, BorderLayout.CENTER);
//        editorFrame.pack();
//        editorFrame.revalidate();
//        editorFrame.repaint();
//
//      }
//    });
//  }
//}
