/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics.pkgpackage;

/**
 *
 * @author Martin
 */
 import javax.swing.JFrame;

public class Shapes
 {
 // execute application
 public static void main( String[] args )
 {
 // create frame for ShapesJPanel
 JFrame frame = new JFrame( "Drawing 2D shapes" );
 frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

 // create ShapesJPanel
 ShapesJPanel shapesJPanel = new ShapesJPanel();

 frame.add( shapesJPanel ); // add shapesJPanel to frame
 frame.setSize( 425, 200 ); // set frame size
 frame.setVisible( true ); // display frame
 } // end main
 } // end class Shapes
